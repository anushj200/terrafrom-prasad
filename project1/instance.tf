resource "aws_instance" "web" {
  ami           = "ami-035be7bafff33b6b6"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.terraform-key.key_name}"
  vpc_security_group_ids = ["${data.aws_security_group.getallsg.id}"]
  tags = {
    Name = "server1-terrafrom"
  }
}

resource "aws_instance" "app" {
  ami           = "ami-035be7bafff33b6b6"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.terraform-key.key_name}"
  vpc_security_group_ids = ["${data.aws_security_group.getallsg.id}"]

  tags = {
    Name = "server2-terrafrom"
  }
}
