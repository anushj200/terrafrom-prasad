resource "aws_key_pair" "terraform-key" {
  key_name   = "terraform"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkHyTzJOGqMAWRIXpgiRtm50HZEYnSnseLxSIx0cQIruLx31HJWkbY7lq/tItws1gzzix9lmgn0H1jdsRKqclSRT1FnB2outTJTLgQBgrZ+/SthzPRW/WVia4MRqYErmueeIM5+2YvYYDDcnktd56iGaMbLbA1ZM00nGcw+azCWL/an7n7d4D2kJRFkoR2aGNxxqCgo/bcUDonqeAV3jYJcYttVqEtrVAOH98p1M1sdaFmaJQkR1j7TBQhaOzxxD1ZqjZY8n6Hjrbe7CNvpO0ZjWoFPUJnuc+4KK1kxCGyE5fwB4+7C2A5c5gW/mg8yNcHDp8josEUtLnM64OBfEDL ubuntu@terraform-box"
}
